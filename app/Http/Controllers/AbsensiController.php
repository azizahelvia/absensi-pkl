<?php

namespace App\Http\Controllers;

use App\Exports\absensiExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller; 

use Illuminate\Http\Request;
use App\Models\Absensi;

class AbsensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abController = Absensi::all();
        return view('lihat-absensi',compact('abController'));
    }

    // public function keluar()
    // {
    //     // $abController = Absensi::all();
    //     return view('absen-keluar');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(request $request)
    {
        return view('absen-masuk');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Absensi::create([
            'nama_lengkap' => $request->nama_lengkap,
            'tanggal' => $request->tanggal,
            'asal_sekolah' => $request->asal_sekolah,
            'jam_masuk' => $request->jam_masuk,
            'jam_keluar' => $request->jam_keluar,
            'keterangan' => $request->keterangan,
            ]);
        
            return redirect('absen-masuk')->with('success', 'Berhasil Ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dtAbsen = Absensi::findorfail($id);
        return view('absen-keluar', compact('dtAbsen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dtAbsen = Absensi::findorfail($id);
        $dtAbsen->update($request->all());
        return view('absen-keluar', compact('dtAbsen'))->with('success', 'Berhasil Absen Keluar!');
        
        return redirect('/lihat-absensi')->with('success', 'Berhasil Ditambahkan!');

        // Absensi::create([
        //     'jam_keluar' => $request->jam_keluar,
        //     'keterangan' => $request->keterangan,
        //     ]);
        
    }

    public function absensiExport($tanggal)
    {
        $abController = Absensi::where('tanggal', $tanggal)->orderBy('tanggal', 'asc')->get();
        return Excel::download(new absensiExport, 'Rekap-Absensi.xlsx');
        // return (new absensiExport)->download('absensi.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function halamanrekap()
    {
        // return view('halaman-rekap-absensi');
        $data = Absensi::all();
        view()->share('abController', $data);
    }

    public function showabsensi($tanggaldari, $tanggalsampai)
    {
        $abController = Absensi::where('tanggal', $tanggaldari, $tanggalsampai)->orderBy('tanggal', 'asc')->get();
        return view('halaman-rekap-absensi', compact('abController'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function presensiPulang(request $request)
    {
        // Absensi::create([
        //     // 'nama_lengkap' => $request->nama_lengkap,
        //     // 'tanggal' => $request->tanggal,
        //     // 'asal_sekolah' => $request->asal_sekolah,
        //     // 'jam_masuk' => $request->jam_masuk,
        //     'jam_keluar' => $request->jam_keluar,
        //     // 'keterangan' => $request->keterangan,
        //     ]);
        
            // $dtAbsen = Absensi::findorfail($id);
            // $dtAbsen->update($request->jam_keluar());

            // $dtAbsen = Absensi::findorfail($request);
            // return view('absen-keluar', compact('dtAbsen'));
            
            // return redirect('absen-keluar')->with('success', 'Berhasil Ditambahkan!');
    }
}
