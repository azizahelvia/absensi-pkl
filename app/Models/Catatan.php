<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catatan extends Model
{
    public $timestamps = false;

    protected $table = "catatan";
    protected $primarykey = "id_catatan";
    protected $fillable = ['id_catatan', 'tanggal', 'catatan'];

    // public function catatan()
    // {
    //     return $this->belongsTo(User::class);
    // }
}
