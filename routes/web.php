<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DaftarController;
use App\Http\Controllers\AbsensiController;
use App\Http\Controllers\CatatanController;


Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);




// route  middleware login dan logout
Route::group(['middleware' => 'auth'], function () {
    
    Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    // route ke halaman lain
    Route::get('/daftar', function() { return view ('daftar'); });
    Route::get('/absen-masuk', function() { return view ('absen-masuk'); });
    Route::get('/absen-keluar', function() { return view ('absen-keluar'); });
    Route::get('/lihat-absensi', function() { return view ('lihat-absensi'); });
    Route::get('/tambah', function() { return view ('tambah'); });
    
    
    //lihat absensi full
    Route::get('lihat-absensi', [AbsensiController::class, 'index'])->name('lihat-absensi');
    
    // absen masuk
    Route::get('absen-masuk', [AbsensiController::class, 'create'])->name('tambah');
    Route::post('absen', [AbsensiController::class, 'store'])->name('absen');
    
    // route absen pulang 
    Route::get('absen-keluar/{id}', [AbsensiController::class, 'edit'])->name('absen-keluar');
    Route::post('presensi-keluar/{id}', [AbsensiController::class, 'update'])->name('presensi-keluar');
    
    //find data tanggal
    Route::get('filter-absen', [AbsensiController::class, 'halamanrekap'])->name('filter-absen');
    Route::get('filter-absen/{tanggaldari}/{tanggalsampai}', [AbsensiController::class, 'showabsensi'])->name('filter-absen-all');
    
    Route::get('/catatan', [CatatanController::class, 'index'])->name('catatan');
    // crud catatan
    Route::get('tambah-catatan', [CatatanController::class, 'create'])->name('tambah-catatan');
    Route::post('simpan-catatan', [CatatanController::class, 'store'])->name('simpan-catatan');
    Route::get('editCatatan/{id}', [CatatanController::class, 'edit'])->name('editCatatan');
    Route::post('update-catatan/{id}', [CatatanController::class, 'update'])->name('update-catatan');
    Route::get('delete-catatan/{id}', [CatatanController::class, 'destroy'])->name('delete-catatan');
});



Route::group(['middleware' => ['auth','ceklevel:admin']], function () {
    
    Route::get('export-absen', [AbsensiController::class, 'absensiExport'])->name('export-absen');
    Route::get('/daftar', [DaftarController::class, 'index']);
    Route::get('tambah', [DaftarController::class, 'create'])->name('tambah');
    Route::post('simpan', [DaftarController::class, 'store'])->name('simpan');
    Route::get('edit/{id}', [DaftarController::class, 'edit'])->name('edit');
    Route::post('update/{id}', [DaftarController::class, 'update'])->name('update');
    Route::get('delete/{id}', [DaftarController::class, 'destroy'])->name('delete');
    // route find tanggal

});






// // absen keluar belom jelas
// Route::get('keluar/{id}', [AbsensiController::class, 'edit'])->name('keluar');
// Route::post('update-absen/{id}', [AbsensiController::class, 'update'])->name('update-absensi');
